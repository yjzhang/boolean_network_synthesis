\documentclass[]{article}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8x]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[unicode=true]{hyperref}
\hypersetup{
            pdftitle={Program Synthesis For Asynchronous Boolean Gene Regulatory Networks},
            pdfauthor={Yue Zhang},
            pdfborder={0 0 0},
            breaklinks=true}
%\urlstyle{same}  % don't use monospace font for urls
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
%\setcounter{secnumdepth}{1}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

% set default figure placement to htbp
\makeatletter
\def\fps@figure{htbp}
\makeatother


\title{Program Synthesis For Asynchronous Boolean Gene Regulatory Networks}
\author{Yue Zhang}

\begin{document}
\maketitle

\section{Introduction}

Gene regulatory networks, or GRNs, are graphs that describe how expression levels of any gene depends on expression levels of other genes. They are useful for many tasks, including identifying key regulatory genes and predicting the effect of mutations. In addition, GRNs can change in different cell types or under different biological conditions, so identifying the changes in the networks can help with understanding the differences between cell types.

There are many different ways to construct gene networks, but one particularly interesting method involves using program synthesis techniques to construct an asynchronous Boolean GRN from binarized data. Program synthesis techniques allow the generation of all possible models that are compatible with a data set. Also, the models produced are executable programs: they can be run in order to look at possible cell fates or the effects of perturbations.

In this report I will describe how boolean GRNs can be constructed using program synthesis. Most of this is based on methods described in \cite{fisher_synthesising_2015} and \cite{moignard_decoding_2015}.


\section{Methods}

\subsection{Asynchronous Boolean GRNs}

Asynchronous Boolean gene networks are comprised of a set of Boolean update functions, one function for each gene. The update functions take as input the Boolean states of all the genes, and return a single value, the new value for the given gene. They are called "asynchronous" because they are executed by updating only one gene at a time, holding all other genes constant. The graph structure of the network comes from the update functions. Each node represents a gene, and there is an edge from one gene to another if the first gene is present in the update function for the second gene. Edges can represent activating or inhibiting relationships. An example network is shown in figure \ref{fig:grn}.

\begin{figure}
    \begin{center}
        \includegraphics[width=10cm]{diagrams/grn}
        \caption{Figure from \cite{garg_synchronous_2008} showing one example of a Boolean GRN. Green edges show activating interactions, while red edges show inhibiting interactions.}
     \label{fig:grn}
     \end{center}
 \end{figure}

\subsection{Data}

Gene networks can be constructed from any kind of single-cell gene expression data. This data consists of a matrix with dimensions $genes \times cells$, with the entries being the expression level of a given gene in a given cell. This data can come from single-cell RNASeq or qRT-PCR.

\subsection{Preprocessing}

First, the data has to be discretized by converting real-valued gene expression values into binary values. In \cite{moignard_decoding_2015}, this is done by setting a threshold based on the detection level of the measurement technology, and setting everything below that to zero and everything above that to one. This can also be done by clustering the expression values for a given gene, and assigning the lower cluster to zero.

The next step is to create a state graph using the binarized data. In this graph, each node represents a cell state (a particular configuration of genes that are on or off), and edges connect cell states that differ only by one. Examples of small state graphs can be seen in \ref{fig:states}. Directions for the edges can be induced by pseudotime orderings of the cells, or through the synthesis process.

\subsection{Encoding}

In order to synthesize the Boolean update functions, they have to be encoded in a way that is feasible for SMT solvers. In \cite{fisher_synthesising_2015}, each function is represented by a list of bit vectors of fixed length. Each bit vector represents either a binary operator or a variable. In my implementation the evaluation is done from left to right, with the operators in prefix notation, and implicit parentheses starting at each operator. The bit width and number of bit vectors per function are provided as inputs and can be used to constrain function complexity. An update function is comprised of a pair of these functions, with one being the activators and the other being the repressors. The update function is thus $f_i(s) = f_{ia}(s) \wedge \neg f_{ir}(s)$. A variable may only be present in the pair once.

\subsection{Constraints}

There are two major constraints involved in synthesis. The first one is that states connected in the state graph have to be reachable. This constraint is encoded by adding an assertion that for each edge in the state graph, when the update function corresponding to the variable that changes is applied to the first state, the output has to be equal to the value of the variable in the second state. In \cite{fisher_synthesising_2015}, the constraint is somewhat more complicated, and is based on having ending states be reachable from starting states through the update functions.

The other constraint basically encodes the idea that updates not part of the data set should not be reached. If a variable is not updated in a particular transition, thenrunning the update function on the starting state should not change the value of the variable in the ending state. This is expressed as a maximization, where we maxmize the sum of the variables that don't change and shouldn't change for each edge in the state graph. in \cite{fisher_synthesising_2015}, this constraint is expressed as a hard threshold.

\section{Results}


\begin{figure}
    \begin{center}
        \includegraphics[width=10cm]{diagrams/simple_states}
        \caption{Some simple state graphs from \cite{garg_synchronous_2008} that were used to test synthesis.}
    \label{fig:states}
    \end{center}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{tabular}{c|c|c}
            Graph & Variable & Update function\\\hline
            b &$v_0$ & No change \\
              &$v_1$ & No change \\
              &$v_2$ & $v_0 \wedge \neg v_2$\\
              &$v_3$ & $v_0 \wedge \neg (v_1 \vee v_2)$\\\hline
            c &$v_0$ & No change\\
              &$v_1$ & $v_2 \wedge \neg v_3$\\
              &$v_2$ & $v_0 \wedge \neg v_2$\\
              &$v_3$ & $v_0 \wedge \neg (v_1 \vee v_2)$\\
        \end{tabular}
        \caption{Learned update functions for the graphs shown in \ref{fig:states}}
    \label{fig:functions}
    \end{center}
\end{figure}

\begin{figure}
    \begin{center}
        \begin{tabular}{c|c|c}
            Graph & Variable & Update function\\\hline
       Actual &$v_0$ & $v_2 \wedge v_3$ \\
              &$v_1$ & $v_0$ \\
              &$v_2$ & $\neg v_1$\\
              &$v_3$ & $\neg v_0$\\\hline
  Synthesized &$v_0$ & $v_2 \wedge \neg v_0$\\
              &$v_1$ & $v_0$\\
              &$v_2$ & $(v_2 \vee v_3) \wedge \neg v_1$\\
              &$v_3$ & $(v_1 \vee v_2) \wedge \neg v_0$\\
        \end{tabular}
        \caption{Actual vs synthesized update functions (based on \cite{lim_btr:_2016})}
    \label{fig:functions2}
    \end{center}
\end{figure}

I implemented a simplified version of the framework described in \cite{fisher_synthesising_2015} in Racket and Rosette as a way to test out the methods. For the first trial, I tested the method on a number of synthetic datasets. These included some simple state graphs, as shown in \ref{fig:states}. In addition, the test dataset included a graph generated by executing a known set of functions. I tested different numbers of bitvectors in order to assess the effect on performance.

The results of learning the functions with three bitvectors are shown in figure~\ref{fig:functions} and figure~\ref{fig:functions2}. While the functions manage to recaptiulate the graphs, they seem much more complicated than they need to be, with many extraneous variables. It is somewhat more difficult to minimize function complexity as opposed to setting a hard constraint. Also, it is unclear that they are minimizing the number of unwanted state transitions.

\begin{figure}
    \begin{center}
\begin{tabular}{ccccccc}
    vars  &  states&  edges &  runtime (s) & number of bitvectors  &  bitwidth &  notes\\\hline
    4 &  14&  24&  58.397 &  3 &  4 &  Known function execution\\
    4 &  14&  24&  7.313  &  2 &  4 &  Known function execution\\
    4 &  14&  24&  1.272  &  1 &  4 &  Known function execution\\
    4 &  6 &  7 &  14.290 &  3 &  4 &  Complex loop\\
    4 &  6 &  7 &  2.585  &  2 &  4 &  Complex loop\\
    4 &  4 &  4 &  0.767  &  1 &  4 &  simple loop\\
    4 &  4 &  4 &  2.285  &  2 &  4 &  simple loop\\
    4 &  4 &  4 &  9.618  &  3 &  4 &  simple loop\\
    2 &  4 &  3 &  0.674  &  1 &  4 &  chain\\
    2 &  4 &  3 &  1.043  &  2 &  4 &  chain\\
    2 &  4 &  3 &  2.724  &  3 &  4 &  chain\\
\end{tabular}
\caption{Runtime performance for various small test datasets}
\label{fig:runtime}
\end{center}
\end{figure}

Runtime performance for a small range of parameters is shown in \ref{fig:runtime}. As would be expected, with larger graphs and more bitvectors per function, the running time increases. The number of bitvectors per function seems to have the largest effect on the runtime. The bitwidth is dependent on the number of variables and cannot be tuned.

\begin{figure}
    \begin{center}
        \includegraphics[width=6cm]{diagrams/sox2_graph}
        \includegraphics[width=6cm]{diagrams/dropseq_threshold_10}
        \includegraphics[width=6cm]{diagrams/dropseq_threshold_20}
        \caption{1. The graph of the network from correlational data in \cite{klein_droplet_2015}. 2. Learned model w/threshold 10 and one bitvector. 3. Learned model 2/threshold 20 and 3 bitvectors. Green arrow=activator, red square=repressor.}
        \label{fig:learned}
    \end{center}
\end{figure}

\begin{figure}
    \begin{center}
\begin{tabular}{ccccccc}
    vars  &  states&  edges &  runtime (s) & number of bitvectors  &  bitwidth &  notes\\\hline
    10& 13 & 42 & 2.932   &  1 &  5 &  Sox2 network w/threshold 20\\
    10& 13 & 42 & 190.855 &  3 &  5 &  Sox2 network w/threshold 20\\
    10& 18 & 63 & 3.618   &  1 &  6 &  Sox2 network w/threshold 10\\
    10& 18 & 63 & >19min  &  3 &  6 &  Sox2 network w/threshold 10, didn't finish\\
    10& 29 & 106& 17.265  &  1 &  7 &  Sox2 network w/threshold 5\\
\end{tabular}
\caption{Runtime performance for real datasets}
\label{fig:realruntime}
\end{center}
\end{figure}

Next, I tried to test the method on some real data. I used the single-cell dataset from \cite{klein_droplet_2015}, which contains the gene expression values for thousands of cells. The set of genes came from the Sox2 network, which contained 10 genes that are known to interact with the gene Sox2 in this dataset. I discretized the data using Poisson clustering, and created a state graph by thresholding the states that have a count of at least 10 cells. This resulted in 18 states and a graph with 68 edges. I also tested different threshold numbers. There were larger graphs and more states with a lower threshold, and the opposite for higher thresholds.

The comparison of two the Boolean networks with a published graph from \cite{klein_droplet_2015} is shown in figure \ref{fig:learned}. The published graph is based on analysis of correlations in gene expression. For the model synthesized from threshold 10 data, out of the nine non-self edges learned, seven of them are also present in the published graph. A hypergeometric test for enrichment of edges versus the correlation-based graph gives a p-value of 0.0674. This is somewhat inconclusive. The model with threshold 20 data performs worse on this metric, with 8 out of 14 edges present in the original graph, and having a hypergeometric test value of 0.55.

The runtime results are shown in figure \ref{fig:realruntime}. As with the test datasets, running took much longer with more bitvectors per function.

I have also tested Karme (\url{https://github.com/koksal/karme}), a much more sophisticated tool for Boolean gene networks. I tested it using the single-cell dataset from \cite{islam_characterization_2011}, selecting the top genes by variance, as well as the same dataset from \cite{klein_droplet_2015} that was used previously. Since Karme always clusters the genes and I can't figure out how to not use clustering, the results are not directly comparible here.

\section{Discussion}

Starting this project I had hoped to make some improvements to Karme, but I did not accomplish this. I ran into trouble due to my limited understanding of the problem, the complexity of the codebase, and the lack of validated data. The Racket/Rosette implementation also probably had a much simpler model.

Program synthesis is not the only way to create Boolean GRNs from data. The method described in \cite{lim_btr:_2016} uses an optimization framework instead. While the two frameworks are probably compatible, it is possible that computing the optimization directly would improve performance relative to using SMT solvers for program synthesis. 

There is also the option of using continuous or probabilistic models. Bayesian networks have also been used to model GRNs, but according to \cite{moignard_decoding_2015}, they only deal with directed acyclic networks. However, there are other graphical models such as factor graphs that can deal with directed networks. It remains to be seen how they compare to Boolean networks.

\section{Code}

The code that was used is available at \url{https://bitbucket.org/yjzhang/boolean_network_synthesis}. This includes code for running the synthesis process, model execution, and some preprocessing steps in Python.

\bibliographystyle{plain}
\bibliography{report}

\end{document}
