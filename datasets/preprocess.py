from collections import Counter

import pandas as pd
import numpy as np

import uncurl

def discretize(data):
    """
    Discretizes a dataset (data is an np array)
    """
    genes, cells = data.shape
    new_data = np.zeros(data.shape)
    for i in range(genes):
        d = data[i,:].reshape((1, cells))
        assignments, means = uncurl.poisson_cluster(d, 2)
        new_data[i,:] = assignments
    return new_data.astype(int)

def build_state_graph(binary_data, count_threshold=4):
    """
    Builds a list of binary states and a state graph.
    """
    genes, cells = binary_data.shape
    binary_data = binary_data.astype(bool)
    state_counter = Counter()
    for c in range(cells):
        state = binary_data[:,c]
        state_counter[tuple(state)] += 1
    all_states = []
    for state, count in state_counter.iteritems():
        if count >= count_threshold:
            all_states.append(state)
    all_states_map = {s:i for i,s in enumerate(all_states)}
    edges = []
    for i, state in enumerate(all_states):
        for v in range(genes):
            new_state = state[:v] + (not state[v],) + state[v+1:]
            try:
                index = all_states_map[new_state]
                edges.append((i, v, index))
            except:
                pass
    return all_states, edges

def print_states_list(states):
    """
    Converts the list of states into a form that's easily copied into racket
    """
    def convert(b):
        if b:
            return '#t'
        else:
            return '#f'
    output = ''
    for s in states:
        output += '('
        output += ' '.join([convert(b) for b in s])
        output += ')\n'
    return output

def print_edges_list(edges):
    output = ''
    for e in edges:
        output += '('
        output += ' '.join([str(s) for s in e])
        output += ')\n'
    return output
