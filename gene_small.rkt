;; Rosette implementation...

#lang rosette/safe   

(require "synthesis.rkt")

;; function encoding: bitvectors - each variable appears only once
;; each bitvector represents either a variable or "and" or "or"
;; if bitv==0: "and"; if bitv=="1": "or"; else: variable bitv - 2
;; 
;; Function list: the function list is comprised of pairs of functions, one 
;; on the activators and one on the repressors.
;;
;; State encoding: a list of booleans

;; list of lists of booleans
(define states1
  (list->vector
  `((#t #f)
    (#t #t)
    (#f #t)
    (#f #f))))

(define states2
  (list->vector
  `((#t #f #f #t)
    (#t #f #t #t)
    (#t #f #t #f)
    (#t #f #f #f)
    ;; complex loop
    (#t #t #f #f)
    (#t #t #t #f))))
;; 0: 1 0 0 1
;; 1: 1 0 1 1
;; 2: 1 0 1 0
;; 3: 1 0 0 0
;; 4: 1 1 0 0
;; 5: 1 1 1 0

;; edge list - starting_state var ending_state
(define graph1
  '((0 1 1)
    (1 0 2)
    (2 1 3)
    ;(3 0 0)
    )
)

(define graph2
  '((0 2 1)
    (1 3 2)
    (2 2 3)
    (3 3 0)
    ;(4 0 5)
    ))

;; complex loop
(define graph3
  '(
    (4 1 3)
    (3 3 0)
    (0 2 1)
    (1 3 2)
    (2 2 3)
    (2 1 5)
    (5 2 4)))

(define nvars 2)
(define nvars2 4)


;; number of bitvectors in a function
(define fnum 1)

(define functions1 (create-functions nvars fnum fsize))
(define functions2 (create-functions nvars2 fnum fsize))

;(displayln functions1)
;(displayln (map (lambda (x) (apply append x)) functions1))
(define f12 (map (lambda (x) (apply append x)) functions1))
(define f22 (map (lambda (x) (apply append x)) functions2))

(define sm1 (build-states-map states1 graph1 nvars))
(define sm2 (build-states-map states2 graph3 nvars2))
(displayln sm1)
(displayln sm2)
(define cn1 (count-negative-match functions1 sm1 states1 graph1 nvars))
(define cn2 (count-negative-match functions2 sm2 states2 graph2 nvars2))
(displayln cn1)
(displayln cn2)

;(define sol
;  (solve (assert (and
;                   (andmap assert-one-gene f22)
;                   (andmap (lambda (e) 
;                             (edge-compatible?
;                               e functions2 states2)) graph3)))))

;(displayln sol)
;(define fvals1 (map (lambda (x) (map (lambda (y) (evaluate y sol)) x)) functions2))
;(displayln fvals1)
;(map (lambda (x) (begin (display "function +: ") (displayln (function-form-0 (car x))) 
;                        (display "function -: ") (displayln (function-form-0 (cadr x))))) fvals1)


(define sol2
  (optimize 
    #:maximize (list cn2)
    #:guarantee (assert (and
                   (andmap assert-one-gene f22)
                   (andmap (lambda (e) 
                             (edge-compatible?
                               e functions2 states2)) graph2)))))
              
(displayln sol2)


(define fvals2 (map (lambda (x) (map (lambda (y) (evaluate y sol2)) x)) functions2))
(displayln fvals2)
(map (lambda (x) (begin (display "function +: ") (displayln (function-form-0 (car x))) 
                        (display "function -: ") (displayln (function-form-0 (cadr x))))) fvals2)

;(displayln (count-negative-match fvals1 sm2 states2 graph3 nvars2))
(displayln (count-negative-match fvals2 sm2 states2 graph3 nvars2))
