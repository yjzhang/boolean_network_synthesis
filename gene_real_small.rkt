#lang rosette/safe

(require "synthesis.rkt")

(bv 13 fsize)

(define states
  (list->vector
  '((#f #t #f #f #f #t #t #t #f #f)
    (#t #f #f #f #f #t #f #f #f #f)
    (#f #t #f #f #f #f #f #t #f #f)
    (#t #t #f #f #f #t #f #t #f #f)
    (#f #t #f #f #f #t #f #f #f #f)
    (#t #t #f #f #f #f #f #f #f #f)
    (#t #f #f #f #f #f #f #f #f #f)
    (#f #f #f #f #f #f #f #f #f #f)
    (#f #t #f #f #f #f #f #f #f #f)
    (#t #t #f #f #f #t #f #f #f #f)
    (#f #t #f #f #f #t #f #t #f #f)
    (#t #t #f #f #f #f #f #t #f #f)
    (#f #f #f #f #f #t #f #f #f #f)
    )))

(define graph
  '((0 6 10)
    (1 0 12)
    (1 1 9)
    (1 5 6)
    (2 0 11)
    (2 5 10)
    (2 7 8)
    (3 0 10)
    (3 5 11)
    (3 7 9)
    (4 0 9)
    (4 1 12)
    (4 5 8)
    (4 7 10)
    (5 0 8)
    (5 1 6)
    (5 5 9)
    (5 7 11)
    (6 0 7)
    (6 1 5)
    (6 5 1)
    (7 0 6)
    (7 1 8)
    (7 5 12)
    (8 0 5)
    (8 1 7)
    (8 5 4)
    (8 7 2)
    (9 0 4)
    (9 1 1)
    (9 5 5)
    (9 7 3)
    (10 0 3)
    (10 5 2)
    (10 6 0)
    (10 7 4)
    (11 0 2)
    (11 5 3)
    (11 7 5)
    (12 0 1)
    (12 1 4)
    (12 5 7)
    ))

(define nvars 10)

;; number of bitvectors in a function
(define fnum 3)

(define functions1 (create-functions nvars fnum fsize))

(define f12 (map (lambda (x) (apply append x)) functions1))

(define sm1 (build-states-map states graph nvars))
(define cn1 (count-negative-match functions1 sm1 states graph nvars))

(define sol2
    (optimize
    #:maximize (list cn1)
    #:guarantee (assert (and
    (andmap assert-one-gene f12)
    (andmap (lambda (e)
    (edge-compatible?
        e functions1 states)) graph)))))

(displayln sol2)

(define fvals2 (map (lambda (x) (map (lambda (y) (evaluate y sol2)) x)) functions1))
(displayln fvals2)
(map (lambda (x) (begin (display "function +: ") (displayln (function-form-0 (car x)))
                                                (display "function -: ") (displayln (function-form-0 (cadr x))))) fvals2)
